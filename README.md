# WebapiAngular
This is my Practice project built on Angular 8, Asp.Net Web Api

Front end is simple Todo List with Authentication.

Web Api has asp.net owin authentication and all Web API controllers has [Authorize] attribute.

Database: MS SQL Server, Full DB schema script is available web api project.

UI side: Ngx-bootstrap datepicker, Models, Reactive forms used.

Web Api Project name: DataApi.

Angular Project Name: azngwebapp.


